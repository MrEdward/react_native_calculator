type CalculateFunction = (a: string, o: string, b: string, key?: string) => void;

interface IButtonsProps {
	screenWidth: number;
	onPress(key: string): void;
	clearLabel: string;
}

interface IButtonProps {
	w?: number;
	h?: number | null;
	text: string;
	backgroundColor?: string;
	textColor?: string;
	onPress(key: string): void;
}