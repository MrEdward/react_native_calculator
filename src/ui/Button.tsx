import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export function Button({ w = 0, h = null, text, backgroundColor = '#333', textColor = 'white', onPress }: IButtonProps) {
	const height = h ?? w;
	const width = w;
	return (
		<View style={[styles.buttonContainer, { width, height }]}>
			<TouchableOpacity onPress={() => onPress(text)} style={[styles.button, { backgroundColor }]}>
				<Text style={[styles.buttonText, { color: textColor }]}>{text}</Text>
			</TouchableOpacity>
		</View>
	)
};

const styles = StyleSheet.create({
	buttonContainer: {
		padding: 10,
	},
	button: {
		width: '100%',
		height: '100%',
		borderRadius: 100,
		alignItems: 'center',
		justifyContent: 'center'
	},
	buttonText: {
		color: 'white',
		fontSize: 30,
	}
});