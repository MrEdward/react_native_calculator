import React from 'react'
import { StyleSheet, View } from 'react-native';
import { Button } from './Button';


export default function Buttons({ screenWidth, onPress, clearLabel }: IButtonsProps) {
	const width = (screenWidth / 4) - 5;

	return (
		<View style={styles.buttonsContainer}>
			<View style={styles.buttonsRow}>
				<Button onPress={onPress} w={width} text={clearLabel} backgroundColor='#a5a5a5' textColor='#000'></Button>
				<Button onPress={onPress} w={width} text='+/-' backgroundColor='#a5a5a5' textColor='#000'></Button>
				<Button onPress={onPress} w={width} text='%' backgroundColor='#a5a5a5' textColor='#000'></Button>
				<Button onPress={onPress} w={width} text='/' backgroundColor='#ff9f0a'></Button>
			</View>
			<View style={styles.buttonsRow}>
				<Button onPress={onPress} w={width} text='7'></Button>
				<Button onPress={onPress} w={width} text='8'></Button>
				<Button onPress={onPress} w={width} text='9'></Button>
				<Button onPress={onPress} w={width} text='x' backgroundColor='#ff9f0a'></Button>
			</View>
			<View style={styles.buttonsRow}>
				<Button onPress={onPress} w={width} text='4'></Button>
				<Button onPress={onPress} w={width} text='5'></Button>
				<Button onPress={onPress} w={width} text='6'></Button>
				<Button onPress={onPress} w={width} text='-' backgroundColor='#ff9f0a'></Button>
			</View>
			<View style={styles.buttonsRow}>
				<Button onPress={onPress} w={width} text='1'></Button>
				<Button onPress={onPress} w={width} text='2'></Button>
				<Button onPress={onPress} w={width} text='3'></Button>
				<Button onPress={onPress} w={width} text='+' backgroundColor='#ff9f0a'></Button>
			</View>
			<View style={styles.buttonsRow}>
				<Button onPress={onPress} w={width * 2} h={width} text='0'></Button>
				<Button onPress={onPress} w={width} text=','></Button>
				<Button onPress={onPress} w={width} text='=' backgroundColor='#ff9f0a'></Button>
			</View>
		</View>
	)
}





const styles = StyleSheet.create({
	buttonsContainer: {
		paddingHorizontal: 10,
		paddingTop:10,
		paddingBottom:30,
		
	},
	buttonsRow: {
		flexDirection: 'row'
	},
});
