import React, { useState } from 'react'
import { StyleSheet, Text, View, useWindowDimensions } from 'react-native';
import Buttons from './src/ui/Buttons';



export default function App() {
	const { width } = useWindowDimensions()
	const [firstValue, setFirstValue] = useState('');
	const [operator, setOperator] = useState('');
	const [secondValue, setSecondValue] = useState('');
	const [clearLabel, setClearLabel] = useState('AC');

	const onKeyPress = (key: string) => {
		switch (key) {
			case 'AC':
				setFirstValue('');
				setOperator('');
				setSecondValue('')
				break;
			case 'C':
				if (secondValue !== '') setSecondValue('');
				else setFirstValue('');
				setClearLabel('AC');
				break;
			case '+/-':
				if (firstValue !== '' || secondValue !== '') {
					if (firstValue !== '' && secondValue === '') {
						setFirstValue((parseFloat(firstValue) * -1).toString())
					} else {
						setSecondValue((parseFloat(secondValue) * -1).toString())
					}
				}
				break;
			case '%':
				calculate(firstValue, key, secondValue)
				break;
			case '/':
			case 'x':
			case '-':
			case '+':
				if (secondValue !== '') calculate(firstValue, operator, secondValue, key)
				else setOperator(key);

				break;
			case '=':
				calculate(firstValue, operator, secondValue)
				break;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '0':
			case ',':
				setClearLabel('C');


				if (operator === '') setFirstValue(prev => prev + key);
				else setSecondValue(prev => prev + key);
				break;

			default:
				break;
		}
	};


	const getDisplayText = () => {
		if (secondValue !== '') return secondValue;
		if (firstValue === '') return '0';

		return firstValue;
	};

	const calculate: CalculateFunction = (a = '', o = '', b = '', key) => {


		let res: number | string = 0;
		if (a === '' || b === '0') {
			setFirstValue('');
			setOperator('');
			setSecondValue('')
			return
		}

		a = a.replace(',', '.');
		b = b.replace(',', '.');

		switch (o) {
			case '%':
				res = parseFloat(a) / 100
				break;
			case '/':

				res = parseFloat(a) / parseFloat(b)
				break;
			case 'x':
				res = parseFloat(a) * parseFloat(b)
				break;
			case '-':
				res = parseFloat(a) - parseFloat(b)
				break;
			case '+':
				res = parseFloat(a) + parseFloat(b)
				break;

			default:
				break;
		}

		if (res % 1 !== 0) {
			const digitValue = res.toString().split('.')[1];
			if (digitValue.length > 6) {
				res = parseFloat(res.toFixed(6))
			}
		}
		res = res.toString().replace('.', ',');

		setFirstValue(res);
		key ? setOperator(key) : setOperator('');
		setSecondValue('');
	};

	return (
		<View style={styles.container}>
			<View style={styles.displayContaiener}>
				<Text style={styles.displayText}>{getDisplayText()}</Text>
			</View>

			<Buttons screenWidth={width} onPress={onKeyPress} clearLabel={clearLabel} />

		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#000'
	},
	displayContaiener: {
		flex: 1,
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
		paddingHorizontal: 30
	},
	displayText: {
		fontSize: 70,
		color: 'white'
	}
});
